package replay;

import static org.apache.beam.sdk.extensions.gcp.util.Transport.getJsonFactory;
import static org.apache.beam.sdk.extensions.gcp.util.Transport.getTransport;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.Collections;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.dataflow.model.Job;
import lombok.extern.slf4j.Slf4j;

import com.google.api.services.dataflow.Dataflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class EventsReplayApplication {
    public static void main(String[] args) throws IOException {
        SpringApplication.run(EventsReplayApplication.class, args);

		final GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream("./gcpcreds.json"))
				.createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));

        final Dataflow dataflowAPI = new Dataflow(
				new NetHttpTransport(),
				JacksonFactory.getDefaultInstance(),
				credential
		);
        final Job result = dataflowAPI
				.projects()
				.jobs()
				.get("Five9DataServices", "2019-11-21_01_26_04-12179049067187363119")
				.execute();

        log.info("Status message: " + result.getCurrentState());
    }
}
